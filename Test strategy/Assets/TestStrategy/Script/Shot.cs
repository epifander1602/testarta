using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : MonoBehaviour
{
    [SerializeField] private Animator anim;
    [SerializeField] private GameObject StartShot;
    [SerializeField] private Ball core;
    [SerializeField] private Transform target;
    [SerializeField] private float TimeActivatod;
 
    private void Update()
    {
        Fire1();
    }

    private void Fire1()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            anim.Play("Start");
            anim.SetTrigger("Clicker");
            Invoke("Fire", TimeActivatod);
        }
    }

    private void Fire()
    {
        Ball cores = Instantiate(core) as Ball;
        cores.transform.position = StartShot.transform.position;
        cores.transform.rotation = StartShot.transform.rotation;
        cores.StartMove(target.position);
    }
}
