using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] private float speed;

    public void StartMove(Vector3 target)
    {
        gameObject.SetActive(true);
        IEnumerator routine = Fly(target);
        StartCoroutine(routine);
    }
    
    private IEnumerator Fly(Vector3 target)
    {
        float time = 0;

        while (time <= 1)
        {
            transform.position = Vector3.Lerp(transform.position, target, time);
            time += speed * Time.deltaTime;
            yield return null;
        }
        Destroy(gameObject);
    }
}
